package com.montibeller.AUselessCalculator;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AUselessCalculatorApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AUselessCalculatorApplication.class, args);
	}
	Calculator calc = new Calculator();


	@Override
	public void run(String... args) throws Exception {
		System.out.println("A useless Calculator");

		System.out.println(calc.calculator());
	}
}

