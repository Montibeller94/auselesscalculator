package com.montibeller.AUselessCalculator;

import java.util.Scanner;

public class Calculator {

    private Scanner scan;

    public Calculator() {
        scan = new Scanner(System.in);
    }

    public double calculator() {
        System.out.println("Geben Sie die erste Zahl ein");
        double zahl1 = scan.nextDouble();
        System.out.println("Geben Sie die zweite Zahl ein");
        double zahl2 = scan.nextDouble();


        double rechnung = zahl1 + zahl2;

        return rechnung;
    }

}
